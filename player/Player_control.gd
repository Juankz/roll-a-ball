extends RigidBody

export var speed = 10

func _physics_process(delta):
	var movement = Vector3()
	
	# Get input
	if(Input.is_action_pressed("ui_up")):
		movement.z=-1
	elif(Input.is_action_pressed("ui_down")):
		movement.z=1
	if(Input.is_action_pressed("ui_left")):
		movement.x=-1
	elif(Input.is_action_pressed("ui_right")):
		movement.x=1
	
	apply_impulse(Vector3(),movement*delta*speed)
	
