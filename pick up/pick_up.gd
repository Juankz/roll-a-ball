extends Area

func _process(delta):
	rotate(Vector3 (15, 30, 45).normalized(),delta)
	
func _on_pickup_body_entered( body ):
	if(body.is_in_group("player")):
		print("collision with player detected")
		get_node("/root/global").score += 1
		get_tree().call_group("ui","score_changed")
		queue_free()
