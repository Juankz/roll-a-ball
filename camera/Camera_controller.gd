extends Camera

var player_ref
var offset

func _ready():
	player_ref = $"../Player"
	offset = transform.origin - player_ref.transform.origin
	pass

func _process(delta):
	transform.origin = player_ref.transform.origin + offset